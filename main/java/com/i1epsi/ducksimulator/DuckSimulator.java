/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.i1epsi.ducksimulator;

/**
 *
 * @author TVall
 */
public class DuckSimulator {

    public static void main(String[] args) {
        simulate();
    }
    
    static void simulate() {
        Quackable Bali = new BaliDuck();
        Quackable Magpie = new MagpieDuck();
        Goose goose = new Goose();
        Goose.honk();
        Bali.quack();
        Magpie.quack();
    }
}

//Pour utiliser l'oie dans simulate il faut le design pattern adapter
//Pour ajouter le nouveau comportement aux canards il faut le design pattern 
//pour fabriquer tous les canards il faut le design pattern
//pour que l'ornithologue soit notifié chaque fois que le canard cancanne il faut le design pattern observer