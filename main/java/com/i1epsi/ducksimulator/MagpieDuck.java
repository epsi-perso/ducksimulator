/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.i1epsi.ducksimulator;

/**
 *
 * @author TVall
 */
public class MagpieDuck implements Quackable{
    public void quack(){
        System.out.println("Magpie Duck say quack quack !!!");
    }
}
