/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.i1epsi.ducksimulator;

/**
 *
 * @author TVall
 */
public interface Quackable {
    void quack();
}
